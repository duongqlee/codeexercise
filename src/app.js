"use strict";

const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

module.exports = (db) => {
  app.use(cors());
  //Initalize data
  let testData = [
    [-43, 96, 7, -154, "Amanda Chen", "Jonathan Miller", "Richard Garner"],
    [17, -143, -20, 89, "Sean Robinson", "Matthew Diaz", "Anne Harris"],
    [54, 119, 69, 168, "Dawn Gray", "Stephanie Martinez", "Albert Taylor"],
    [-32, 7, -54, 88, "Erika Schwartz", "Lisa Gonzalez", "Erin Barnes"],
    [62, 138, 45, -3, "Megan Jackson", "Melissa Smith", "Jennifer Dixon"],
    [25, -31, 27, 81, "Luke Carlson", "Cody Lang", "Heather Craig"],
    [85, 35, -24, -95, "Allison Wells", "Brenda Johnson", "Mark Smith"],
    [-39, -3, -30, 23, "Michael Spencer", "Frank White", "Robert Bennett"],
    [-17, -36, -44, 95, "Melanie Evans", "Troy Cooper", "Jacob Wheeler"],
    [-48, -61, 56, 164, "Mark Ramirez", "Calvin Jacobs", "Kylie Hubbard"],
    [17, -108, 63, -53, "Peter Watson", "Tammy Alexander", "Juan Williams"],
    [-1, 40, 68, 87, "Joshua George", "Tiffany Joyce", "James Thompson"],
  ];

  let flatData = [];
  testData.forEach((record) => {
    record.forEach((field) => {
      flatData.push(field);
    });
  });
  let placeHolder = testData.map(() => "(?, ?, ?, ?, ?, ?, ?)").join(", ");

  let intializeDataQuery =
    "INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES " +
    placeHolder;
  db.run(intializeDataQuery, flatData, (err) => {
    if (err) throw err;
  });

  app.get("/health", (req, res) => res.send("Healthy"));

  app.post("/rides", jsonParser, (req, res) => {
    const startLatitude = Number(req.body.start_lat);
    const startLongitude = Number(req.body.start_long);
    const endLatitude = Number(req.body.end_lat);
    const endLongitude = Number(req.body.end_long);
    const riderName = req.body.rider_name;
    const driverName = req.body.driver_name;
    const driverVehicle = req.body.driver_vehicle;

    if (
      startLatitude < -90 ||
      startLatitude > 90 ||
      startLongitude < -180 ||
      startLongitude > 180
    ) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message:
          "Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
      });
    }

    if (
      endLatitude < -90 ||
      endLatitude > 90 ||
      endLongitude < -180 ||
      endLongitude > 180
    ) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message:
          "End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
      });
    }

    if (typeof riderName !== "string" || riderName.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    if (typeof driverName !== "string" || driverName.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    if (typeof driverVehicle !== "string" || driverVehicle.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    var values = [
      req.body.start_lat,
      req.body.start_long,
      req.body.end_lat,
      req.body.end_long,
      req.body.rider_name,
      req.body.driver_name,
      req.body.driver_vehicle,
    ];

    const result = db.run(
      "INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES (?, ?, ?, ?, ?, ?, ?)",
      values,
      function (err) {
        if (err) {
          return res.send({
            error_code: "SERVER_ERROR",
            message: "Unknown error",
          });
        }

        db.all(
          "SELECT * FROM Rides WHERE rideID = ?",
          this.lastID,
          function (err, rows) {
            if (err) {
              return res.send({
                error_code: "SERVER_ERROR",
                message: "Unknown error",
              });
            }

            res.send(rows);
          }
        );
      }
    );
  });

  app.get("/rides", async (req, res) => {
    try {
      const result = await getAllRides(
        parseInt(req.query["page"]),
        parseInt(req.query["page_size"])
      );
      res.send(result);
    } catch (err) {
      res.send(err);
    }
  });

  async function getAllRides(page, page_size) {
    return new Promise((resolve, reject) => {
      db.all("SELECT * FROM Rides", function (err, rows) {
        if (err) {
          reject({
            error_code: "SERVER_ERROR",
            message: "Unknown error",
          });
        }

        if (rows.length === 0) {
          reject({
            error_code: "RIDES_NOT_FOUND_ERROR",
            message: "Could not find any rides",
          });
        }

        if (!page) page = 1;
        if (!page_size) page_size = 10;
        const num_pages = Math.ceil(rows.length / page_size);
        resolve({
          page: page,
          page_size: page_size,
          num_pages: num_pages,
          results: rows.slice(page * page_size - page_size, page * page_size),
        });
      });
    });
  }

  app.get("/rides/:id", async (req, res) => {
    try {
      const result = await getRideById(req.params.id);
      res.status(200).send(result);
    } catch (err) {
      res.status(400).send(err);
    }
  });

  async function getRideById(id) {
    return new Promise((resolve, reject) => {
      db.all(`SELECT * FROM Rides WHERE rideID='${id}'`, function (err, rows) {
        if (err) {
          reject({
            error_code: "SERVER_ERROR",
            message: "Unknown error",
          });
        }

        if (rows.length === 0) {
          reject({
            error_code: "RIDES_NOT_FOUND_ERROR",
            message: "Could not find any rides",
          });
        }

        if (rows.length === 1) {
          resolve(rows[0]);
        }
      });
    });
  }

  return app;
};
